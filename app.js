var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {

    //新user
    socket.on('add user', function (msg) {
        socket.username = msg;
        console.log("new user:" + msg + " logged.");
        io.emit('add user', {
            username: socket.username
        });
    });

    //監聽新訊息事件
    socket.on('chat message', function (msg) {

        console.log(socket.username + "：" + msg);

        //發佈新訊息
        io.emit('chat message', {
            username: socket.username,
            msg: msg
        });
    });

    //left
    socket.on('disconnect', function () {
        console.log(socket.username + " left.");
        io.emit('user left', {
            username: socket.username
        });
    });


});

//指定port
http.listen(port, function () {
    console.log('listening on *:' + port);
});